# Calibration

Calibration uses the projector to project a circles grid. A 3D camera
(realsense) is used to find the 3D object points of the projected pattern.
Finally OpenCVs stereoCalibrate estimates the projectors intrinsic and
extrinsic parameters.

# Attribution
- The simple workflow for calibrating a projector via OpenCV: 
http://www.morethantechnical.com/2017/11/17/projector-camera-calibration-the-easy-way/
- ARSandbox for the idea of using a 3D camera to capture object points:
https://arsandbox.ucdavis.edu/
- Later found this article which does basically the same:
https://bingyaohuang.github.io/Calibrate-Kinect-and-projector/